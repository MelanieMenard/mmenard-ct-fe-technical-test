import React, { Component } from 'react';
import './App.css';
import { SimpleCaesarCipher } from './components/simple-caesar-cipher';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <section className="App-title-wrapper">
            <h1 className="App-title">
              Welcome to <span className="strikethrough">React</span> Caesar Cipher
            </h1>
          </section>
        </header>
        <main className="App-content">
          <SimpleCaesarCipher />
        </main>
      </div>
    );
  }
}

export default App;
