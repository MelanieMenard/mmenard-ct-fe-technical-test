import cipherUtils from "./cipherUtils";

describe("cipherUtils", () => {

  describe("encrypt", () => {

    it("Encodes 'HELLO' to 'IFMMP' with a key of 1", () => {
      // arrange
      const expected = "IFMMP";

      // act
      const key = 1;
      const input = "HELLO";
      const actual = cipherUtils.encrypt(input, key);

      // assert
      expect(actual).toEqual(expected);
    });

    it("Wraps the end of the alphabet to the beginning", () => {

      const expected = "YZABCD";

      const key = 1;
      const input = "XYZABC";
      const actual = cipherUtils.encrypt(input, key);

      expect(actual).toEqual(expected);
    });

    it("Wraps the beginning of the alphabet to the end with a negative key", () => {

      const expected = "ZABC";

      const key = -1;
      const input = "ABCD";
      const actual = cipherUtils.encrypt(input, key);

      expect(actual).toEqual(expected);
    });

    it("Leaves string unchanged with a key of 0", () => {

      const expected = "XYZABC";

      const key = 0;
      const input = "XYZABC";
      const actual = cipherUtils.encrypt(input, key);

      expect(actual).toEqual(expected);
    });

    it("Leaves all characters unchanged apart from uppercase letters", () => {

      const expected = "1zA./ 2aB;)";

      const key = 1;
      const input = "1zZ./ 2aA;)";
      const actual = cipherUtils.encrypt(input, key);

      expect(actual).toEqual(expected);
    });

    it("Takes the modulus of the key when a positive key is greater than the alphabet length", () => {

      const expected = "YZABCD";

      const key = 27;
      const input = "XYZABC";
      const actual = cipherUtils.encrypt(input, key);

      expect(actual).toEqual(expected);
    });

    it("Takes the modulus of the key when a negative key is greater than the alphabet length", () => {

      const expected = "ZABC";

      const key = -27;
      const input = "ABCD";
      const actual = cipherUtils.encrypt(input, key);

      expect(actual).toEqual(expected);
    });

  });

  describe("decrypt", () => {

    it("Decodes 'IFMMP' to 'HELLO' with a key of 1", () => {
      // arrange
      const expected = "HELLO";

      // act
      const key = 1;
      const input = "IFMMP";
      const actual = cipherUtils.decrypt(input, key);

      // assert
      expect(actual).toEqual(expected);
    });

    it("Wraps the end of the alphabet to the beginning", () => {

      const expected = "XYZABC";

      const key = 1;
      const input = "YZABCD";
      const actual = cipherUtils.decrypt(input, key);

      expect(actual).toEqual(expected);
    });

    it("Wraps the beginning of the alphabet to the end with a negative key", () => {

      const expected = "ABCD";

      const key = -1;
      const input = "ZABC";
      const actual = cipherUtils.decrypt(input, key);

      expect(actual).toEqual(expected);
    });

    it("Leaves string unchanged with a key of 0", () => {

      const expected = "XYZABC";

      const key = 0;
      const input = "XYZABC";
      const actual = cipherUtils.decrypt(input, key);

      expect(actual).toEqual(expected);
    });

    it("Leaves all characters unchanged apart from uppercase letters", () => {

      const expected = "1zZ./ 2aA;)";

      const key = 1;
      const input = "1zA./ 2aB;)";
      const actual = cipherUtils.decrypt(input, key);

      expect(actual).toEqual(expected);
    });

    it("Takes the modulus of the key when a positive key is greater than the alphabet length", () => {

      const expected = "XYZABC";

      const key = 27;
      const input = "YZABCD";
      const actual = cipherUtils.decrypt(input, key);

      expect(actual).toEqual(expected);
    });

    it("Takes the modulus of the key when a negative key is greater than the alphabet length", () => {

      const expected = "ABCD";

      const key = -27;
      const input = "ZABC";
      const actual = cipherUtils.decrypt(input, key);

      expect(actual).toEqual(expected);
    });

  });
  
});
