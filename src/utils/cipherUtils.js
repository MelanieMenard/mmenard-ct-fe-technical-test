const firstCharCode = 'A'.charCodeAt();
const lastCharCode = 'Z'.charCodeAt();
const alphabetLength = lastCharCode - firstCharCode + 1;

const encrypt = (input, key) => {
  const modulusKey = key % alphabetLength;
  const encodedString = input.split('').map((char) => {
    if (char.match(/[A-Z]/)) {
      let shiftedCharCode = char.charCodeAt() + modulusKey;
      if (shiftedCharCode > lastCharCode) {
        shiftedCharCode -= alphabetLength;
      }
      else if (shiftedCharCode < firstCharCode) {
        shiftedCharCode += alphabetLength;
      }
      return String.fromCharCode(shiftedCharCode);
    }
    return char;
  }).join('');
  return encodedString;
};

const decrypt = (input, key) => {
  const modulusKey = key % alphabetLength;
  const decodedString = input.split('').map((char) => {
    if (char.match(/[A-Z]/)) {
      let shiftedCharCode = char.charCodeAt() - modulusKey;
      if (shiftedCharCode > lastCharCode) {
        shiftedCharCode -= alphabetLength;
      }
      else if (shiftedCharCode < firstCharCode) {
        shiftedCharCode += alphabetLength;
      }
      return String.fromCharCode(shiftedCharCode);
    }
    return char;
  }).join('');
  return decodedString;
};

export default {
    encrypt,
    decrypt,
}
