import React, { Component, Fragment } from 'react';
import cipherUtils from '../utils/cipherUtils';
// MM: that would be the proper way to include component CSS however git does not pick it up
// and I see nothing in gitignore telling it to ignore css files so I have no idea why
// I put the component CSS in App.css to keep going
// import './simple-caesar-cipher.css';

class SimpleCaesarCipher extends Component {

  constructor(props) {
    super(props);
    this.state = {
      inputText: 'HELLO WORLD',
      key: 1,
      mode: 'encode',
    };
    this.onInputChange = this.onInputChange.bind(this);
  }

  onInputChange(e) {
    const stateKey = e.target.name;
    // the number input returns a string
    const value = (stateKey === 'key') ? parseInt(e.target.value, 10) : e.target.value;
    // input can temporarily lead to NaN when the user is typing (empty box, minus sign)
    // in that case don't update the controlled input
    // for a real app we may invest the time to do something more user friendly to allow typing negative numbers
    // currently you can only enter negative numbers with the arrows
    // same behaviour as this demo: https://cryptii.com/pipes/caesar-cipher)
    if ((stateKey === 'key') && Number.isNaN(value)) {
      return;
    }
    this.setState({
      [stateKey]: value,
    });
  }

  render() {

    const mode = this.state.mode;
    const key = this.state.key;

    const outputText = (mode === 'encode')
      ? cipherUtils.encrypt(this.state.inputText, key)
      : cipherUtils.decrypt(this.state.inputText, key);

    return (
      <form className="caesar-cipher">

        <section className="caesar-cipher-mode-select">
          <label htmlFor="encodemode">
            <input type="radio" id="encodemode" name="mode" value="encode" checked={this.state.mode === 'encode'} onChange={this.onInputChange} />
            Encode
          </label>
          <label htmlFor="decodemode">
            <input type="radio" id="decodemode" name="mode" value="decode" checked={this.state.mode === 'decode'} onChange={this.onInputChange} />
            Decode
          </label>
        </section>

        {(mode === 'encode') ? (

          <Fragment>
            <label htmlFor="cleartext">
              Clear text:
              <input type="text" id="cleartext" name="inputText" value={this.state.inputText} onChange={this.onInputChange} />
            </label>
            <label htmlFor="key">
              Key:
              <input type="number" id="key" name="key" value={this.state.key} onChange={this.onInputChange} />
            </label>
            <label htmlFor="ciphertext">
              Cipher Text:
              <input type="text" id="ciphertext" name="ciphertext" value={outputText} readOnly />
            </label>
          </Fragment>

        ) : (
          <Fragment>
            <label htmlFor="ciphertext">
              Cipher Text:
              <input type="text" id="ciphertext" name="inputText" value={this.state.inputText} onChange={this.onInputChange} />
            </label>
            <label htmlFor="key">
              Key:
              <input type="number" id="key" name="key" value={this.state.key} onChange={this.onInputChange} />
            </label>
            <label htmlFor="cleartext">
              Clear text:
              <input type="text" id="cleartext" name="cleartext" value={outputText} readOnly />
            </label>
          </Fragment>
        )}
      </form>
    );
  }
}

export { SimpleCaesarCipher };
